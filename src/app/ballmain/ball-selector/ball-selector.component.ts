import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Ball, Winner } from '../bet-slip/bet-slip.component';

@Component({
  selector: 'app-ball-selector',
  templateUrl: './ball-selector.component.html',
  styleUrls: ['./ball-selector.component.scss']
})
export class BallSelectorComponent {

  form: FormGroup = new FormGroup({
    amount: new FormControl(null, [Validators.required, Validators.min(5)])
  });
  @Output() confirmBet = new EventEmitter<Winner>();
  private _selectedBalls: Ball[] = [
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
    { color: 'default', isSelected: false },
  ];
  @Input() set setBall(b: Ball) {
    if (b && !this._selectedBalls.find(ball => ball.id === b.id)) {
      this._selectedBalls.pop();
      this._selectedBalls = [b, ...this._selectedBalls];
      this.form.get('amount').setValue(null);
      this.betAmount = 0;
      this.form.markAsPristine();
      this.form.markAsUntouched();
    }
  }

  get isValid()
  {
    return !this.form.valid && (this.form.touched || this.form.dirty)
  }

  get bet() {
    return this.form.get('amount').value
  };
  betAmount = 0;

  get selectedBalls(): Ball[] {
    return this._selectedBalls;
  }

  get selectedCount(): number {
    return this.selectedBalls.filter(b => b.isSelected === true).length
  }

  setBet(): void {
    if (this.bet > 5) {
      this.betAmount = this.bet * this.selectedCount;
    }
  }

  get enableSetBet() {
    return this.selectedCount > 0;
  }

  get enableConfirmBet() {
    return this.bet > 5 && this.enableSetBet;
  }

  placeBet(): void {
    if (this.betAmount) {
      const random = this.randomInteger(1, 10);
      const betBall = this.selectedBalls.find(b => b.id === random)
      if (betBall)
      {
        const winner = {
          winner: betBall,
          amount: (this.bet * 1.5) - this.betAmount
        } as Winner;
        this.confirmBet.emit(winner);
      }
      else
      {
        alert("You lose :(")
      }
    }
  }

  randomInteger(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
