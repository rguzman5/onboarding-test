import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Ball } from '../bet-slip/bet-slip.component';

@Component({
  selector: 'app-ball',
  templateUrl: './ball.component.html',
  styleUrls: ['./ball.component.scss']
})
export class BallComponent {

  @Input() balls: Ball[];
  @Input() size: 'xl';
  @Output() selectBall =  new EventEmitter<Ball>();
  getColorClass(color: string): string {
    return `ball-container__ball--${color}`
  }

  setBall(ball: Ball): void
  {
    this .selectBall.emit(ball)
  }

}
