import { Component } from '@angular/core';
import { Ball, Winner } from '../bet-slip/bet-slip.component';

@Component({
  selector: 'app-ball-main',
  templateUrl: './ball-main.component.html',
  styleUrls: ['./ball-main.component.scss']
})
export class BallMainComponent {

  selectedBall!: Ball;
  winner: Winner;

  setUserSelection(b: Ball): void
  {
    this.selectedBall = b;
  }

  setWiner(w: Winner): void
  {
    this.winner = w;
  }

}
