import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BallMainComponent } from './ball-main.component';

describe('BallMainComponent', () => {
  let component: BallMainComponent;
  let fixture: ComponentFixture<BallMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BallMainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BallMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
