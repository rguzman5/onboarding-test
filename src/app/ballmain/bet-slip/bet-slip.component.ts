import { Component, EventEmitter, Input, Output } from '@angular/core';

export interface Ball {
  id?: number,
  color: string;
  isSelected?: boolean;
}

export interface Winner {
  winner: Ball,
  amount: number;
}

@Component({
  selector: 'app-bet-slip',
  templateUrl: './bet-slip.component.html',
  styleUrls: ['./bet-slip.component.scss']
})
export class BetSlipComponent {

  @Output() ballSelected = new EventEmitter<Ball>();
  @Input() winner: Winner; 
  balls: Ball[] = [
    { id: 1, color: 'red', isSelected: false},
    { id: 2, color: 'blue', isSelected: false},
    { id: 3, color: 'green', isSelected: false},
    { id: 4, color: 'skyblue', isSelected: false},
    { id: 5, color: 'yellow', isSelected: false},
    { id: 6, color: 'red', isSelected: false},
    { id: 7, color: 'blue', isSelected: false},
    { id: 8, color: 'green', isSelected: false},
    { id: 9, color: 'skyblue', isSelected: false},
    { id: 10, color: 'yellow', isSelected: false},
  ]
  ball: Ball;

  getColorClass(color: string): string
  {
    return `ball-container__ball--${color}`
  }

  selectBall(b: Ball): void
  {
    const ball = {
      ...b,
      isSelected: true
    }
    this.ball = ball;
    this.ballSelected.emit(ball)
  }
}
