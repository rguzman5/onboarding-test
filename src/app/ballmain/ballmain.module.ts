import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BallSelectorComponent } from './ball-selector/ball-selector.component';
import { BetSlipComponent } from './bet-slip/bet-slip.component';
import { BallComponent } from './ball/ball.component';
import { BallMainComponent } from './ball-main/ball-main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BallmainRoutingModule } from './ballmain-routing.module';



@NgModule({
  declarations: [
    BallSelectorComponent,
    BetSlipComponent,
    BallComponent,
    BallMainComponent],
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    BallmainRoutingModule,
  ],
  exports: [BallSelectorComponent, BetSlipComponent, BallComponent, BallMainComponent,]
})
export class BallmainModule { }
