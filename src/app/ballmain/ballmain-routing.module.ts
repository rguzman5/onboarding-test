import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BallMainComponent } from './ball-main/ball-main.component';

const routes: Routes = [
  { path: '', redirectTo: 'ball-main', pathMatch: 'full' },
  { path: 'ball-main', component: BallMainComponent }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class BallmainRoutingModule { }
