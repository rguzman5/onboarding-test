import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BallmainModule } from './ballmain/ballmain.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BallmainModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
